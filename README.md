# GoWild Website Version 1.0

## Installation

Requires:
- node.js and was built using v6.10.2.
- mongodb - refer seperately for collections

Also recommended to have bower and karma installed globally. `npm install bower karma -g`

You will also need to create a `./src/config` folder, and inside create the config file `config.js`. The emails are being sent from the contact form using AWS SES, so the SES details should be stored in a module in this file.

```
// ./src/config/config.js

const config = {

  // secret for cookie-parser
  "cookieSecret": "secretCookieString",

  // db connection string
  "connectionString": "connectionStringToMongoDb",

  // aws ses details
  "awsSes": {
    "key": "awsKey",
    "secret": "awsSecret",
    "amazon": "aswServerEndpoint"
  }
};

module.exports = config;
```

Now open terminal and navigate to the root of the repo, then download the required packages.

```npm install```

## Usage

ensure your database is running

```mongod --dbpath <path to your database>```

open a second terminal tab or window

To start the website in development mode, type

```gulp serve-dev```

and this should serve the site to your browser at http://localhost:3000.

Alternatively, typing `gulp` will bring up a list of other available tasks that may be useful.

After the karma runner has run for the first time (this will happen as part of regular builds or can be called specifically), you can find a 'Coverage' folder in the root directory. This contains a HTML file that reports on unit test coverage.

## History

This site was created to support a small business. To advertise the products and services, and allow a method of contact.

## Credits

Michael Browning

michael.f.browning@gmail.com
