(function () {
  'use strict';

  var app = angular.module('goWildHire');

  app.directive('bookNow', function () {
    return {
      restrict: 'E',
      replace: true,
      template: '<div class="book-now btn-4" ui-sref="contact">Book now</div>'
    };
  });
}());
