(function () {
  'use strict';

  var app = angular.module('goWildHire');

  app.directive('productGrid', function () {
    return {
      restrict: 'E',
      replace: true,
      templateUrl: '/app/gowild-hire/gowild-productGrid/gowild-productGrid.html'
    };
  });
}());
