(function () {
  'use strict';

  var app = angular.module('goWildHire');

  app.directive('productList', function () {
    return {
      restrict: 'E',
      replace: true,
      templateUrl: '/app/gowild-hire/gowild-productList/gowild-productList.html'
    };
  });
}());
