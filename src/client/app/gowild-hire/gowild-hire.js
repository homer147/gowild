(function () {
  'use strict';

  var app = angular.module('goWildHire', ['ui.router']);

  app.config(['$stateProvider', '$locationProvider',
    function ($stateProvider, $locationProvider) {
      $stateProvider
        .state('hire', {
          url: '/hire',
          template: '<h1>Hire</h1>'
        })
        .state('popcorn', {
          url: '/hire/floss-popcorn',
          templateUrl: '/app/gowild-hire/gowild-hire.html',
          controller: 'popcornCtrl'
        })
        .state('slushie', {
          url: '/hire/slushie',
          templateUrl: '/app/gowild-hire/gowild-hire.html',
          controller: 'slushieCtrl'
        })
        .state('castles', {
          url: '/hire/castles',
          templateUrl: '/app/gowild-hire/gowild-hire.html',
          controller: 'castlesCtrl'
        })
        .state('singleCastle', {
          url: '/hire/castles/:name',
          templateUrl: '/app/gowild-hire/gowild-singleCastle/gowild-singleCastle.html',
          controller: 'castlesCtrl'
        });

      $locationProvider.html5Mode(true);
    }
  ]);
}());
