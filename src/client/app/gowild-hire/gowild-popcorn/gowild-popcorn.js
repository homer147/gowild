(function () {
  'use strict';

  var app = angular.module('goWildHire');

  app.controller('popcornCtrl', ['$scope', '$sce', '$state', 'productFactory',
    function ($scope, $sce, $state, productFactory) {
      var init;

      $scope.trustAsHtml = $sce.trustAsHtml;
      $scope.page = $state.current.name;

      init = function (res) {
        $scope.intro = res.data.intro;
        $scope.products = res.data.products;
        $scope.rawflavours = res.data.rawflavours;
      };

      productFactory.getFairyFlossAndPopcornData(function (res) {
        init(res);
      });
    }]);
}());
