(function () {
  'use strict';

  describe('slushie page flavours directive', function () {
    var compile,
      element,
      compiledElement,
      directiveElem,
      scope;

    function getCompiledElement() {
      element = angular.element('<gowild-flavours></gowild-flavours>');
      compiledElement = compile(element)(scope);
      scope.$digest();
      return compiledElement;
    }

    beforeEach(function() {
      module('templates');
      module('goWildApp');
    });

    beforeEach(inject(function($compile, $rootScope) {
      compile = $compile;
      scope = $rootScope.$new();
    }));

    beforeEach(function() {
      directiveElem = getCompiledElement();
    });

    it('should successfully create directive object', function() {
      expect(directiveElem).toBeDefined();
      expect(scope.openKidsFlavours).toBeFalsy();
      expect(scope.openFlavours).toBeFalsy();
    });

    it('should toggle the kids flavours list', function () {
      spyOn(scope, '$apply');

      scope.toggleKidsFlavours();
      expect(scope.openKidsFlavours).toBeTruthy();

      scope.toggleKidsFlavours();
      expect(scope.openKidsFlavours).toBeFalsy();

      expect(scope.$apply).toHaveBeenCalled();
    });

    it('should toggle the adults flavours list', function () {
      spyOn(scope, '$apply');

      scope.toggleFlavours();
      expect(scope.openFlavours).toBeTruthy();

      scope.toggleFlavours();
      expect(scope.openFlavours).toBeFalsy();

      expect(scope.$apply).toHaveBeenCalled();
    });

  });
}());
