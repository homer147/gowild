(function () {
  'use strict';

  var app = angular.module('goWildHire');

  app.directive('gowildFlavours', ['$rootScope', function ($rootScope) {
    return {
      restrict: 'E',
      replace: true,
      templateUrl: '/app/gowild-hire/gowild-flavours/gowild-flavours.html',
      link: function (scope) {
        scope.openKidsFlavours = false;
        scope.openFlavours = false;

        function trackFlavoursClick(eventAction) {
          $rootScope.$emit('clickEvent', {
            eventCategory: 'click',
            eventAction: eventAction,
            eventLabel: 'slushie-page'
          });
          scope.$apply();
        }

        scope.toggleKidsFlavours = function () {
          scope.openKidsFlavours = !scope.openKidsFlavours;
          if (scope.openKidsFlavours) {
            trackFlavoursClick('kids-flavours-open');
          }
        };

        scope.toggleFlavours = function () {
          scope.openFlavours = !scope.openFlavours;
          if (scope.openFlavours) {
            trackFlavoursClick('adult-flavours-open');
          }
        };
      }
    };
  }]);
}());
