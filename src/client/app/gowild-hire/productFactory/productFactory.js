(function () {
  'use strict';

  var app = angular.module('goWildHire');

  app.factory('productFactory', ['$http', function ($http) {
    var getCastleData,
      castleData,
      getSlushieData,
      slushieData,
      getFairyFlossAndPopcornData,
      fairyFlossAndPopcornData;

    getCastleData = function (cb) {
      if (!castleData) {
        $http.get('/api/getCastles').then(function (res) {
          castleData = res;
          cb(castleData);
        });
      } else {
        cb(castleData);
      }
    };

    getSlushieData = function (cb) {
      if (!slushieData) {
        $http.get('/api/getSlushies').then(function (res) {
          slushieData = res;
          cb(slushieData);
        });
      } else {
        cb(slushieData);
      }
    };

    getFairyFlossAndPopcornData = function (cb) {
      if (!fairyFlossAndPopcornData) {
        $http.get('/api/getPopcorn').then(function (res) {
          fairyFlossAndPopcornData = res;
          cb(fairyFlossAndPopcornData);
        });
      } else {
        cb(fairyFlossAndPopcornData);
      }
    };

    return {
      getCastleData: getCastleData,
      getSlushieData: getSlushieData,
      getFairyFlossAndPopcornData: getFairyFlossAndPopcornData
    };
  }]);
}());
