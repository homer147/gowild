(function () {
  'use strict';

  describe('hire product factory', function () {
    var productFactory,
      $httpBackend,
      jsonData;

    beforeEach(function() {
      module('goWildHire');
    });

    beforeEach(inject(function (_productFactory_, $injector) {
      productFactory = _productFactory_;
      $httpBackend = $injector.get('$httpBackend');
      jasmine.getJSONFixtures().fixturesPath = 'base/src/client/app/gowild-hire/productFactory';
      jsonData = getJSONFixture('productFactory.spec.json');
    }));

    it('should ensure productFactory exist', function() {
      expect(productFactory).toBeDefined();
    });

    it('should get the castle data', function () {
      var callback;

      $httpBackend.when('GET', '/api/getCastles').respond(jsonData.castles);

      callback = function (res) {
        expect(res.data).toEqual(jsonData.castles);
      };

      productFactory.getCastleData(callback);
      $httpBackend.flush();
    });

    it('should get the slushie data', function () {
      var callback;

      $httpBackend.when('GET', '/api/getSlushies').respond(jsonData.slushie);

      callback = function (res) {
        expect(res.data).toEqual(jsonData.slushie);
      };

      productFactory.getSlushieData(callback);
      $httpBackend.flush();
    });

    it('should get the fairy floss and popcorn data', function () {
      var callback;

      $httpBackend.when('GET', '/api/getPopcorn').respond(jsonData.popcorn);

      callback = function (res) {
        expect(res.data).toEqual(jsonData.popcorn);
      };

      productFactory.getFairyFlossAndPopcornData(callback);
      $httpBackend.flush();
    });
  });
}());
