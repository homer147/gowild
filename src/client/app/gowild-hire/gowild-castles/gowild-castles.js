(function () {
  'use strict';

  var app = angular.module('goWildHire');

  app.controller('castlesCtrl', ['$scope', '$state', 'productFactory', '$sce',
    function ($scope, $state, productFactory, $sce) {
      $scope.page = $state.current.name;
      $scope.product = '';
      $scope.showPackages = false;

      productFactory.getCastleData(function (res) {
        $scope.intro = res.data.intro;
        $scope.products = res.data.products;

        if ($scope.page === 'singleCastle') {
          var slug = $state.params.name;

          for (var i = 0; i < $scope.products.length; i++) {
            if ($scope.products[i].slug === slug) {
              $scope.product = $scope.products[i];
              break;
            }
          }
        }
      });

      $scope.productRoute = function (castle) {
        if (castle && castle.slug) {
          $state.go('singleCastle', { name: castle.slug });
        }
      };

      $scope.trust = $sce.trustAsHtml;

      $scope.togglePackages = function () {
        if ($scope.product.packages.length > 0) {
          $scope.showPackages = !$scope.showPackages;
        }
      };

    }]);

  app.directive('clickable', ['$parse', '$compile', function ($parse, $compile) {
    return {
      restrict: 'A',
      link: function (scope, elem, attrs) {
        scope.$watch(attrs.content, function () {
          elem.html($parse(attrs.content)(scope));
          $compile(elem.contents())(scope);
        });
      }
    };
  }]);
}());
