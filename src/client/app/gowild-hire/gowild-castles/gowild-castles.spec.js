(function () {
  'use strict';

  describe('hire castles controller', function () {
    var productFactoryMock,
      createController,
      cb,
      scope,
      stateMock,
      jsonData,
      castlesCtrl;

    beforeEach(function() {
      module('goWildHire');
    });

    beforeEach(function() {
      productFactoryMock = {
        getCastleData: jasmine.createSpy().and.callFake(function() {
          cb = arguments[0];
          cb({ data: jsonData });
        })
      };

      stateMock = {
        current: {
          name: 'singleCastle'
        },
        params: {
          name: 'paw-patrol'
        },
        go: jasmine.createSpy().and.callFake(function () {
          return;
        })
      };
    });

    beforeEach(inject(function ($rootScope, $controller) {
      jasmine.getJSONFixtures().fixturesPath = 'base/src/client/app/gowild-hire/gowild-castles/';
      jsonData = getJSONFixture('gowild-castles.spec.json');

      createController = function() {
        scope = $rootScope.$new();

        return $controller('castlesCtrl', {
          $scope: scope,
          $state: stateMock,
          productFactory: productFactoryMock
        });
      };

      castlesCtrl = createController();
    }));

    it('should set default scope variables', function () {
      expect(scope.intro).toBeDefined();
      expect(scope.products).toBeDefined();
      expect(scope.page).toEqual('singleCastle');
      expect(scope.showPackages).toBeFalsy();
      expect(scope.product).toEqual(jsonData.products[0]);
    });

    it('should route to a single castle page', function () {
      scope.productRoute(scope.product);
      expect(stateMock.go).toHaveBeenCalled();
    });

    it('should toggle the package details popup', function () {
      scope.togglePackages();
      expect(scope.showPackages).toBeTruthy();

      scope.togglePackages();
      expect(scope.showPackages).toBeFalsy();

      scope.product.packages = [];
      scope.togglePackages();
      expect(scope.showPackages).toBeFalsy();
    });
  });
}());
