(function () {
  'use strict';

  var app = angular.module('goWildHire');

  app.controller('slushieCtrl', ['$scope', '$sce', '$state', 'generalFactory', 'productFactory',
    function ($scope, $sce, $state, generalFactory, productFactory) {
      var init;

      $scope.trustAsHtml = $sce.trustAsHtml;
      $scope.page = $state.current.name;

      init = function (res) {
        $scope.intro = res.data.intro;
        $scope.products = res.data.products;
        $scope.rawflavours = res.data.rawflavours;

        $scope.kidsFlavours = generalFactory.chunk($scope.rawflavours.kidsFlavours, 2);

        $scope.flavours = generalFactory.chunk($scope.rawflavours.cocktails, 2);
      };

      productFactory.getSlushieData(function (res) {
        init(res);
      });
    }]);
}());
