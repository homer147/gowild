(function (){
  'use strict';

  describe('Contact Validation directive', function() {
    var compile,
      scope,
      element,
      spans,
      inputs,
      textArea,
      $httpBackend,
      jsonData,
      html,
      contactForm,
      spanContents,
      compiledElement,
      directiveElem,
      i;

    function getCompiledElement() {
      element = angular.element(html);
      compiledElement = compile(element)(scope);
      scope.$digest();
      return compiledElement;
    }

    beforeEach(function() {
      module('goWildContact');
    });

    beforeEach(inject(function($compile, $rootScope, $injector) {
      compile = $compile;
      scope = $rootScope.$new();
      $httpBackend = $injector.get('$httpBackend');
      jasmine.getJSONFixtures().fixturesPath = 'base/src/client/app/gowild-contact/validation';
      jsonData = getJSONFixture('validation.spec.json');
      html = jsonData.html;
    }));

    beforeEach(function() {
      directiveElem = getCompiledElement();
      contactForm = scope.contactForm;
    });

    it('should successfully create directive object', function() {
      expect(directiveElem).toBeDefined();
    });

    it('should validate an empty contact form', function() {
      spans = directiveElem.find('span');
      spyOn(scope, 'validateMessage').and.callThrough();
      spyOn(scope, 'validateField').and.callThrough();

      expect(contactForm.$invalid).toBeTruthy();

      scope.validateForm(contactForm);

      expect(scope.firstNameError).toBeTruthy();
      expect(scope.lastNameError).toBeTruthy();
      expect(scope.phoneError).toBeTruthy();
      expect(scope.emailError).toBeTruthy();
      expect(scope.messageError).toBeTruthy();
      expect(scope.validateMessage).toHaveBeenCalled();
      expect(scope.validateField.calls.count()).toBe(4);

      for(i = 0; i < spans.length; i++) {
        spanContents = angular.element(spans[i]).text();
        expect(spanContents).toContain('Please enter');
      }
    });

    it('should clear field errors on focus', function() {
      inputs = directiveElem.find('input');
      textArea = directiveElem.find('textarea');
      spans = directiveElem.find('span');

      angular.element(inputs[3]).val('joij').trigger('input');
      scope.validateField(contactForm.email);

      expect(scope.emailError).toBeTruthy();
      expect(angular.element(spans[3]).text()).toContain('Please enter');

      scope.fieldFocus(contactForm.email);

      expect(scope.emailError).toBeFalsy();
      expect(angular.element(spans[3]).text()).not.toContain('Please enter');

      angular.element(textArea[0]).val('joij').trigger('input');
      scope.validateMessage(contactForm.message);

      expect(scope.messageError).toBeTruthy();
      expect(angular.element(spans[4]).text()).toContain('Please enter');

      scope.fieldFocus(contactForm.message);

      expect(scope.messageError).toBeFalsy();
      expect(angular.element(spans[4]).text()).not.toContain('Please enter');
    });
  });
}());
