(function () {
  'use strict';

  var app = angular.module('goWildContact');

  app.directive('validation', function () {
    return function (scope) {
      var errorField,
        input,
        inputLabel,
        inputName,
        fields,
        isEmail,
        errorMsg,
        i;

      scope.validateForm = function (form) {
        if (form.$invalid) {
          fields = form.$$controls;

          for (i = 0; i < fields.length; i++) {
            if (fields[i].$$attr.name !== 'message') {
              scope.validateField(fields[i]);
            } else {
              scope.validateMessage(fields[i]);
            }
          }
        }
      };

      scope.fieldFocus = function (field) {
        input = field.$$element[0];
        errorField = input.nextElementSibling;
        inputName = field.$$attr.name + 'Error';
        errorField.textContent = null;
        scope[inputName] = false;
      };

      scope.validateField = function (field) {
        errorMsg = null;
        input = field.$$element[0];
        errorField = input.nextElementSibling;
        inputName = field.$$attr.name + 'Error';
        isEmail = field.$$attr.placeholder.toLowerCase() === 'youremail@yourdomain.com';
        inputLabel = isEmail ? 'email address' : field.$$attr.placeholder.toLowerCase();

        if (field.$error.pattern || field.$error.minlength || field.$error.maxlength) {
          errorMsg = 'Please enter a valid ' + inputLabel;
          errorField.textContent = errorMsg;
          scope[inputName] = true;
        }

        if (field.$error.required) {
          errorMsg = 'Please enter your ' + inputLabel;
          errorField.textContent = errorMsg;
          scope[inputName] = true;
        }

      };

      scope.validateMessage = function (field) {
        errorMsg = null;
        input = field.$$element[0];
        errorField = input.nextElementSibling;
        inputName = field.$$attr.name + 'Error';

        if (field.$error.required || field.$error.minlength) {
          errorMsg = 'Please enter a message of at least 50 Characters';
          errorField.textContent = errorMsg;
          scope[inputName] = true;
        }

      };
    };
  });
}());
