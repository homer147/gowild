(function(){
  'use strict';

  describe('Contact page controller', function() {
    var mailServiceMock,
      contactFormMock,
      mockSendMailResponse,
      contactCtrl,
      rootScope,
      scope,
      scopeShareServiceMock,
      success,
      jsonData,
      modelMock,
      badEmailModelMock,
      createController;

    beforeEach(function() {
      module('goWildApp');
    });

    beforeEach(function() {
      module(function($provide) {
        $provide.service('mailService', mailServiceMock);
      });
      module(function($provide) {
        $provide.service('scopeShareService', scopeShareServiceMock);
      });
    });

    beforeEach(function() {
      mailServiceMock = {
        sendMail: jasmine.createSpy().and.callFake(function() {
          success = arguments[1];
          success(mockSendMailResponse);
        })
      };

      scopeShareServiceMock = {
        getSendingValue: jasmine.createSpy().and.callFake(function () {
          return false;
        }),
        putSendingValue: jasmine.createSpy().and.callFake(function () {
          return false;
        }),
        registerSendingObservers: jasmine.createSpy()
      };

      contactFormMock = {
        $valid: true
      };
    });

    beforeEach(inject(function($rootScope, $controller) {
      jasmine.getJSONFixtures().fixturesPath = 'base/src/client/app/gowild-contact';
      jsonData = getJSONFixture('gowild-contact.spec.json');
      mockSendMailResponse = jsonData.SendMailResponse;
      modelMock = jsonData.model;
      badEmailModelMock = jsonData.badModelEmail;

      createController = function() {
        rootScope = $rootScope.$new();
        scope = $rootScope.$new();
        scope.contactForm = contactFormMock;

        return $controller('contactCtrl', {
          $scope: scope,
          $rootScope: rootScope,
          mailService: mailServiceMock,
          scopeShareService: scopeShareServiceMock
        });
      };

      contactCtrl = createController();
      spyOn(rootScope, '$emit');
    }));

    it('should set default variables', function() {
      expect(scope.model).toEqual({});
      expect(scope.sending).toBeFalsy();
      expect(scope.numbersOnlyPattern).toEqual(/^[\d\s]+$/);
      expect(scope.emailPattern).toEqual(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
      expect(scopeShareServiceMock.registerSendingObservers).toHaveBeenCalledWith(jasmine.any(Function));
    });

    it('should close the mail sent confirm popup', function() {
      scope.closeSentConfirm();
      expect(scope.confirmSent).toEqual(false);
    });

    it('should submit the form', function() {
      scope.model = modelMock;
      jasmine.createSpy(mailServiceMock, 'sendMail');

      scope.submitForm();

      expect(mailServiceMock.sendMail).toHaveBeenCalled();
      expect(scope.model).toEqual({});
      expect(scope.confirmSent).toEqual(true);
      expect(scopeShareServiceMock.putSendingValue).toHaveBeenCalledWith(false);
      expect(scope.sending).toEqual(false);
      expect(rootScope.$emit).toHaveBeenCalledWith('clickEvent', jasmine.any(Object));
    });

    it('should not submit the form due to bad data', function() {
      scope.model = badEmailModelMock;
      scope.contactForm.$valid = false;
      jasmine.createSpy(mailServiceMock, 'sendMail');

      scope.submitForm();

      expect(mailServiceMock.sendMail).not.toHaveBeenCalled();
      expect(scope.model).toEqual(badEmailModelMock);
      expect(scope.confirmSent).toBeFalsy();
      expect(scope.sending).toEqual(false);
      expect(rootScope.$emit).not.toHaveBeenCalledWith('clickEvent', jasmine.any(Object));
    });
  });
}());
