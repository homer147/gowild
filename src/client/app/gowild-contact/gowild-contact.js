(function () {
  'use strict';

  var app = angular.module('goWildContact', ['ui.router']);

  app.config(['$stateProvider', '$locationProvider',
    function ($stateProvider, $locationProvider) {
      $stateProvider
        .state('contact', {
          url: '/contact',
          templateUrl: '/app/gowild-contact/gowild-contact.html',
          controller: 'contactCtrl'
        });

      $locationProvider.html5Mode(true);
    }
  ]);

  app.controller('contactCtrl',
    ['$scope', '$rootScope', '$state', 'mailService', 'scopeShareService', 'generalFactory',
    function ($scope, $rootScope, $state, mailService, scopeShareService, generalFactory) {
      var updateSendingVar;

      $scope.model = generalFactory.getFromSessionStorage('formData') || {};
      $scope.sending = false;
      $scope.numbersOnlyPattern = /^[\d\s]+$/;
      $scope.emailPattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

      updateSendingVar = function () {
        $scope.sending = scopeShareService.getSendingValue();
      };

      $scope.$on('$stateChangeStart', function () {
        generalFactory.putInSessionStorage('formData', $scope.model);
      });

      $scope.closeSentConfirm = function () {
        $scope.confirmSent = false;
      };

      $scope.submitForm = function () {
        $scope.sending = scopeShareService.putSendingValue(true);
        $scope.firstName = $scope.model.firstName;

        if ($scope.contactForm.$valid) {
          mailService.sendMail($scope.model, function (res) {
            $scope.sending = scopeShareService.putSendingValue(false);

            if (res.data.sent) {
              $scope.model = {};
              $scope.confirmSent = true;
              $rootScope.$emit('clickEvent', {
                eventCategory: 'submit',
                eventAction: 'success',
                eventLabel: 'contact-form'
              });
            } else {
              alert('error occured');
              $rootScope.$emit('clickEvent', {
                eventCategory: 'submit',
                eventAction: 'fail',
                eventLabel: 'contact-form'
              });
            }
          },

          function (res) {
            alert('error occured');
            console.log(res);
            $scope.sending = scopeShareService.putSendingValue(false);
            $rootScope.$emit('clickEvent', {
              eventCategory: 'submit',
              eventAction: 'fail',
              eventLabel: 'contact-form'
            });
          });
        } else {
          $scope.sending = scopeShareService.putSendingValue(false);
        }
      };

      scopeShareService.registerSendingObservers(updateSendingVar);
    }]);
}());
