(function () {
  'use strict';

  var app = angular.module('goWildContact');

  app.service('mailService', ['$http', function ($http) {

    function serialiseObj(obj) {
      var result = [];

      for (var property in obj) {
        if (obj.hasOwnProperty(property)) {
          result.push(encodeURIComponent(property) + '=' + encodeURIComponent(obj[property]));
        }
      }

      return result.join('&');
    }

    var sendMail = function (model, success, error) {

      var params = {
        firstName: model.firstName,
        lastName: model.lastName,
        phone: model.phone,
        email: model.email,
        message: model.message
      };

      $http({
        method: 'POST',
        url: '/api/sendMail',
        data: serialiseObj(params),
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
        }
      })
      .then(function (res) {
        success(res);
      },

      function (res) {
        error(res);
      });
    };

    return {
      sendMail: sendMail
    };

  }]);
}());
