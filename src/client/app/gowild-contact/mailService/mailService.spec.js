(function() {
  'use strict';

  describe('Contact mail service', function() {

    var $httpBackend,
      jsonData,
      mailService,
      mockSendMailResponse,
      mockSendMailBadResponse,
      success,
      error;

    beforeEach(function() {
      module('goWildContact');
    });

    beforeEach(inject(function(_mailService_, $injector) {
      mailService = _mailService_;
      $httpBackend = $injector.get('$httpBackend');
      jasmine.getJSONFixtures().fixturesPath = 'base/src/client/app/gowild-contact';
      jsonData = getJSONFixture('gowild-contact.spec.json');
      mockSendMailResponse = jsonData.SendMailResponse;
      mockSendMailBadResponse = jsonData.SendMailBadResponse;
    }));

    it('should ensure mailService exist', function() {
      expect(mailService).toBeDefined();
    });

    it('should send mail to the backend and receive success response', inject(function($httpBackend) {
      $httpBackend.when('POST', '/api/sendMail').respond(201, mockSendMailResponse);

      success = function(res) {
        expect(res.data.status).toEqual('success');
      };

      mailService.sendMail(jsonData.model, success, error);

      expect($httpBackend.flush).not.toThrow();
    }));

    it('should send mail to the backend and receive error response', inject(function($httpBackend) {
      $httpBackend.when('POST', '/api/sendMail').respond(500, mockSendMailBadResponse);

      error = function(res) {
        expect(res.data.status).toEqual('Internal Server Error');
      };

      mailService.sendMail(jsonData.model, success, error);

      expect($httpBackend.flush).not.toThrow();
    }));

  });
}());
