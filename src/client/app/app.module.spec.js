(function(){
  'use strict';

  describe('goWildApp run block', function () {

  });

  describe('goWildCtrl Controller', function () {
    var rootScope,
      scope,
      createController,
      goWildCtrl,
      trackMock,
      scopeShareServiceMock,
      stateMock;

    beforeEach(function () {
      module('goWildApp');
      module('goWildAnalytics');
      module('goWildCore');
    });

    beforeEach(function () {
      module(function ($provide) {
        $provide.value('$state', stateMock = {
          current: {
            name: 'home'
          }
        });
      });

      module(function ($provide) {
        $provide.service('track', trackMock);
      });
    });

    beforeEach(function () {
      trackMock = {
        pageView: jasmine.createSpy(),
        clickEvent: jasmine.createSpy()
      };

      scopeShareServiceMock = {
        getSendingValue: jasmine.createSpy().and.callFake(function () {
          return false;
        }),
        registerSendingObservers: jasmine.createSpy()
      };
    });

    beforeEach(inject(function ($rootScope, $controller) {
      createController = function () {
        rootScope = $rootScope.$new();
        scope = $rootScope.$new();

        return $controller('goWildCtrl', {
          $scope: scope,
          $rootScope: rootScope,
          track: trackMock,
          scopeShareService: scopeShareServiceMock
        });
      };

      goWildCtrl = createController();
    }));

    it('should have called scopeShareService and registered as an observer', function () {
      expect(scope.sending).toEqual(false);
      expect(scopeShareServiceMock.getSendingValue).toHaveBeenCalled();
      expect(scopeShareServiceMock.registerSendingObservers).toHaveBeenCalledWith(jasmine.any(Function));
    });

    it('should catch $stateChangeSuccess and perform operations', function () {
      rootScope.$emit('$stateChangeSuccess');

      expect(scope.stateName).toEqual('home');
      expect(document.body.scrollTop).toEqual(0);
      expect(trackMock.pageView).toHaveBeenCalled();
    });

    it('should catch clickEvent and call tracking', function () {
      rootScope.$emit('clickEvent', {
        eventCategory: 'submit',
        eventAction: 'success',
        eventLabel: 'contact-form'
      });

      expect(trackMock.clickEvent).toHaveBeenCalledWith('submit', 'success', 'contact-form');
    });
  });

}());
