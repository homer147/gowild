(function () {
  'use strict';

  var app = angular.module('goWildCore');

  app.directive('gowildFooter', function () {
    return {
      restrict: 'E',
      replace: true,
      templateUrl: '/app/gowild-core/gowild-footer/gowild-footer.html'
    };
  });
}());
