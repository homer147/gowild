(function () {
  'use strict';

  var app = angular.module('goWildCore');

  app.directive('gowildHeader', ['$rootScope', '$document', function ($rootScope, $document) {
    return {
      restrict: 'E',
      replace: true,
      templateUrl: '/app/gowild-core/gowild-header/gowild-header.html',
      link: function (scope, elem) {
        var ul = angular.element(elem[0].getElementsByClassName('unordered-list')),
          mm = angular.element(elem[0].getElementsByClassName('mobile-menu')),
          documentClick;

        documentClick = function (event) {
          var target = event.target;

          while ((target !== ul[0]) &&
            (target && target.parentNode !== ul[0]) &&
            (target && target.parentNode !== mm[0])) {
            target = target.parentNode;
          }

          if (!target) {
            scope.showMenu = false;
            scope.$apply();
          }
        };

        scope.trackFb = function () {
          $rootScope.$emit('clickEvent', {
            eventCategory: 'click',
            eventAction: 'fb-link',
            eventLabel: 'home-page'
          });
        };

        $document.on('click', documentClick);

        scope.$on('destroy', function () {
          $document.off('click', documentClick);
        });
      }
    };
  }]);
}());
