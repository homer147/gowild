(function () {
  'use strict';

  var app = angular.module('goWildCore');

  app.directive('gowildSpinner', function () {
    return {
      restrict: 'E',
      templateUrl: '/app/gowild-core/gowild-spinner/gowild-spinner.html',
    };
  });
}());
