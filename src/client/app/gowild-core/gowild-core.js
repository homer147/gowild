(function () {
  'use strict';

  var app = angular.module('goWildCore', ['ui.router', 'ngSanitize']);

  app.config(['$stateProvider', '$urlRouterProvider', '$locationProvider',
    function ($stateProvider, $urlRouterProvider, $locationProvider) {
      $urlRouterProvider.otherwise(function ($injector, $location) {
        var state = $injector.get('$state');
        state.go('404');
        return $location.path();
      });

      $stateProvider
        .state('home', {
          url: '/',
          templateUrl: '/app/gowild-core/gowild-home/gowild-home.html',
          controller: 'homeCtrl'
        })
        .state('about', {
          url: '/about',
          templateUrl: '/app/gowild-core/gowild-about/gowild-about.html'
        })
        .state('404', {
          templateUrl: '/app/gowild-core/gowild-404/gowild-404.html'
        });

      $locationProvider.html5Mode(true);
    }
  ]);
}());
