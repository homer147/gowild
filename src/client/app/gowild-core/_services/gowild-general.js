(function () {
  'use strict';

  var app = angular.module('goWildCore');

  app.factory('generalFactory', function () {
    var chunk,
      putInSessionStorage,
      getFromSessionStorage;

    chunk = function (arr, columns) {

      if (columns < 1 || typeof columns !== 'number') {
        columns = 1;
      }

      var rest = arr.length % columns, // how much to divide
        restUsed = rest, // to keep track of the division over the elements
        partLength = Math.floor(arr.length / columns),
        result = [];

      for (var i = 0; i < arr.length; i += partLength) {
        var end = partLength + i,
          add = false;

        if (rest !== 0 && restUsed) {
          end++;
          restUsed--;
          add = true;
        }

        result.push(arr.slice(i, end));

        if (add) {
          i++;
        }
      }

      return result;
    };

    putInSessionStorage = function (name, value) {
      sessionStorage[name] = JSON.stringify(value);
    };

    getFromSessionStorage = function (name) {
      var storedItem = sessionStorage[name];

      if (storedItem) {
        return JSON.parse(storedItem);
      }

      return null;
    };

    return {
      chunk: chunk,
      putInSessionStorage: putInSessionStorage,
      getFromSessionStorage: getFromSessionStorage
    };

  });
}());
