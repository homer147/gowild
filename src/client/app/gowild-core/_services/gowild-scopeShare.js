(function () {
  'use strict';

  var app = angular.module('goWildCore');

  app.service('scopeShareService', function () {
    var sending = false,
      getSendingValue,
      putSendingValue,
      registerSendingObservers,
      notifySendingObservers,
      sendingObserverCallbacks = [];

    getSendingValue = function () {
      return sending;
    };

    putSendingValue = function (newValue) {
      sending = newValue;
      notifySendingObservers();
      return sending;
    };

    registerSendingObservers = function (callback) {
      sendingObserverCallbacks.push(callback);
    };

    notifySendingObservers = function () {
      angular.forEach(sendingObserverCallbacks, function (callback) {
        callback();
      });
    };

    return {
      getSendingValue: getSendingValue,
      putSendingValue: putSendingValue,
      registerSendingObservers: registerSendingObservers
    };
  });

}());
