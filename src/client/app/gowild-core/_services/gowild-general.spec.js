(function() {
  'use strict';

  describe('GeneralFactory Service', function() {
    var generalFactory,
      $httpBackend,
      result,
      jsonData,
      remainder,
      expectedGroups,
      array,
      model;

    beforeEach(function() {
      module('goWildCore');
    });

    beforeEach(inject(function(_generalFactory_, $injector) {
      generalFactory = _generalFactory_;
      $httpBackend = $injector.get('$httpBackend');
      jasmine.getJSONFixtures().fixturesPath = 'base/src/client/app/gowild-core/_services';
      jsonData = getJSONFixture('gowild-general.spec.json');
      array = jsonData.array;
      model = jsonData.model;
    }));

    it('should ensure generalFactory exist', function() {
      expect(generalFactory).toBeDefined();
    });

    it('should chunk an array into smaller pieces', function() {
      result = generalFactory.chunk(array, 3);
      expectedGroups = array.length / 3;
      expectedGroups = parseInt(Math.floor(expectedGroups));
      remainder = parseInt(array.length % 3);

      expect(result).toEqual(jasmine.any(Array));
      expect(result.length).toEqual(3);
      expect(result[0].length).toEqual(expectedGroups + remainder);
      expect(result[1].length && result[2].length).toEqual(expectedGroups);

      result = generalFactory.chunk(array, -3);
      expect(result.length).toEqual(1);

      result = generalFactory.chunk(6, 3);
      expect(result.length).toEqual(0);
    });

    it('should store form data in sessionStorage', function () {

    });

    it('should retrieve form data from sessionStorage', function () {

    });

  });
}());
