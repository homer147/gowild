(function () {
  'use strict';

  var app = angular.module('goWildCore');

  app.directive('contactButton', function () {
    return {
      restrict: 'E',
      replace: true,
      templateUrl: '/app/gowild-core/gowild-contactButton/gowild-contactButton.html',
      controller: 'homeCtrl',
      link: function (scope, elem) {
        var contactButton = elem[0].getElementsByClassName('contact-button');
        var contactModal = elem[0].getElementsByClassName('contact-modal');
        scope.contactRequest = function () {
          angular.element(contactButton).addClass('hide');
          angular.element(contactModal).removeClass('hide');
          scope.trackClick('contact-us');
        };

        scope.closeContact = function () {
          angular.element(contactModal).addClass('hide');
          angular.element(contactButton).removeClass('hide');
        };
      }
    };
  });
}());
