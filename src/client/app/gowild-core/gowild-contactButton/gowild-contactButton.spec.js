(function() {
  'use strict';

  describe('Home page contact button directive', function() {
    var compile,
      element,
      compiledElement,
      directiveElem,
      contactButton,
      contactModal,
      scope;

    function getCompiledElement() {
      element = angular.element('<contact-button></contact-button>');
      compiledElement = compile(element)(scope);
      scope.$digest();
      return compiledElement;
    }

    beforeEach(function() {
      module('templates');
      module('goWildApp');
    });

    beforeEach(inject(function($compile, $rootScope) {
      compile = $compile;
      scope = $rootScope.$new();
    }));

    beforeEach(function() {
      directiveElem = getCompiledElement();
      contactButton = angular.element(directiveElem[0].getElementsByClassName('contact-button'));
      contactModal = angular.element(directiveElem[0].getElementsByClassName("contact-modal"));
    });

    it('should successfully create directive object', function() {
      expect(directiveElem).toBeDefined();
      expect(contactButton.hasClass('hide')).toBeFalsy();
      expect(contactModal.hasClass('hide')).toBeTruthy();
    });

    it('should open contact modal', function() {
      scope.contactRequest();

      expect(contactButton.hasClass('hide')).toBeTruthy();
      expect(contactModal.hasClass('hide')).toBeFalsy();
    });

    it('should close contact modal once opened', function() {
      scope.contactRequest();

      expect(contactButton.hasClass('hide')).toBeTruthy();
      expect(contactModal.hasClass('hide')).toBeFalsy();

      scope.closeContact();
      expect(contactButton.hasClass('hide')).toBeFalsy();
      expect(contactModal.hasClass('hide')).toBeTruthy();
    });
  });
}());
