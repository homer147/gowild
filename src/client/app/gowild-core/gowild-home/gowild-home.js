(function () {
  'use strict';

  var app = angular.module('goWildCore');

  app.controller('homeCtrl', ['$rootScope', '$scope', '$state',
    function ($rootScope, $scope, $state) {
      $scope.go = function (state, eventAction) {
        $rootScope.$emit('clickEvent', {
          eventCategory: 'click',
          eventAction: eventAction,
          eventLabel: 'home-page',
          hitCallback: $state.go(state)
        });
      };

      $scope.trackClick = function (eventAction) {
        $rootScope.$emit('clickEvent', {
          eventCategory: 'click',
          eventAction: eventAction,
          eventLabel: 'home-page'
        });
      };
    }]);
}());
