(function(){
  'use strict';

  describe('goWildAnalytics service', function () {
    var track,
      windowMock,
      locationMock,
      $rootScope;

    beforeEach(function() {
      module('goWildAnalytics', function ($provide) {
        locationMock = {
          url: jasmine.createSpy().and.returnValue('/contact')
        };
        $provide.value('$location', locationMock);

        windowMock = {
          ga: jasmine.createSpy()
        };
        $provide.value('$window', windowMock);
      });
    });

    beforeEach(inject(function (_track_, _$rootScope_) {
      track = _track_;
      $rootScope = _$rootScope_;
    }));

    it('should ensure track service exists', function () {
      expect(track).toBeDefined();
    });

    it('should track a pageview event', function () {
      track.pageView();
      expect(windowMock.ga).toHaveBeenCalledWith('send', jasmine.objectContaining({
        hitType: 'pageview',
        page: '/contact'
      }));
    });

    it('should track a click event', function () {
      track.clickEvent('click', 'contact-us', 'home-page');
      expect(windowMock.ga).toHaveBeenCalledWith('send', jasmine.objectContaining({
        hitType: 'event',
        eventCategory: 'click',
        eventAction: 'contact-us',
        eventLabel: 'home-page'
      }));
    });

    it('should not track if all params are not strings', function () {
      track.clickEvent(4, {}, 'homer');
      expect(windowMock.ga).not.toHaveBeenCalled();
    });
  });
}());
