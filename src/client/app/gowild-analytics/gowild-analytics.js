(function () {
  'use strict';

  var app = angular.module('goWildAnalytics', []);

  app.service('track', ['$rootScope', '$window', '$location',
    function ($rootScope, $window, $location) {
      var pageView,
        clickEvent;

      pageView = function () {
        $window.ga('send', {
          hitType: 'pageview',
          page: $location.url()
        });
      };

      clickEvent = function (eventCategory, eventAction, eventLabel) {
        var string1 = typeof eventCategory === 'string',
          string2 = typeof eventAction === 'string',
          string3 = typeof eventLabel === 'string';

        if (string1 && string2 && string3) {
          $window.ga('send', {
            hitType: 'event',
            eventCategory: eventCategory,
            eventAction: eventAction,
            eventLabel: eventLabel
          });
        }
      };

      return {
        pageView: pageView,
        clickEvent: clickEvent
      };
    }]);
}());
