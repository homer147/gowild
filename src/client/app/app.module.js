(function () {
  'use strict';

  var app = angular.module('goWildApp',
    [

      // lib modules
      'ngAria',

      // my modules
      'goWildCore',
      'goWildContact',
      'goWildHire',
      'goWildAnalytics'
    ]
  );

  app.run(function () {

    if ('serviceWorker' in navigator) {
      navigator.serviceWorker.register('/service-worker.js')
        .then(function (reg) {
          // registration worked
          console.log('Registration succeeded. Scope is ' + reg.scope);
          reg.onupdatefound = function () {
            var installingWorker = reg.installing;

            installingWorker.onstatechange = function () {
              switch (installingWorker.state) {
                case 'installed':
                  if (navigator.serviceWorker.controller) {
                    console.log('New or updated content is available.');
                  } else {
                    console.log('Content is now available offline!');
                  }

                  break;

                case 'redundant':
                  console.error('The installing service worker became redundant.');
                  break;
              }
            };
          };
        }).catch(function (error) {
          // registration failed
          console.log('Registration failed with ' + error);
        });
    }
  });

  app.controller('goWildCtrl', ['$scope', '$rootScope', '$state', 'track', 'scopeShareService',
    function ($scope, $rootScope, $state, track, scopeShareService) {
      var updateSendingVar,
        cleanupStateChangeSuccessListener,
        cleanupClickEventListener;

      $scope.sending = scopeShareService.getSendingValue();

      updateSendingVar = function () {
        $scope.sending = scopeShareService.getSendingValue();
      };

      cleanupStateChangeSuccessListener = $rootScope.$on('$stateChangeSuccess', function () {
        $scope.stateName = $state.current.name;
        document.body.scrollTop = document.documentElement.scrollTop = 0;
        track.pageView();
      });

      cleanupClickEventListener = $rootScope.$on('clickEvent', function (event, data) {
        track.clickEvent(data.eventCategory, data.eventAction, data.eventLabel);
      });

      scopeShareService.registerSendingObservers(updateSendingVar);

      $scope.$on('destroy', function () {
        cleanupStateChangeSuccessListener();
        cleanupClickEventListener();
      });
    }]);
}());
