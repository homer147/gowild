/* jslint node: true */
'use strict';

const NodeCache = require('node-cache');
const gwCache = new NodeCache({ stdTTL: 100, checkperiod: 120 });

module.exports = gwCache;
