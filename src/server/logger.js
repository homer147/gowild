/*jslint node: true */
'use strict';

var log4js = require('log4js');
var logger = log4js.getLogger('gowild');

logger.debug('This little thing went to market');
logger.info('This little thing stayed at home');
logger.error('This little thing had roast beef');
logger.fatal('This little thing had none');
logger.trace('and this little thing went wee, wee, wee, all the way home.');

log4js.configure({
  appenders: {
    file: {
      type: 'file',
      filename: 'log4js/gowild.log',
      maxLogSize: 10 * 1024 * 1024, // = 10Mb
      numBackups: 5, // keep five backup files
      compress: true, // compress the backups
      encoding: 'utf-8',
    },
    dateFile: {
      type: 'dateFile',
      filename: 'log4js/gowild-datefile.log',
      pattern: 'yyyy-MM-dd-hh',
      compress: true
    },
    out: {
      type: 'stdout'
    }
  },
  categories: { default: { appenders: ['file', 'dateFile', 'out'], level: 'debug' } }
});

module.exports = logger;
