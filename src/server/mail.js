/*jslint node: true */
'use strict';

const ses = require('node-ses');
const config = require('../config/config');
const client = ses.createClient(config.awsSes);
const logger = require('./logger');

var mail = {};

function validEmail(email) {
  let pattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  email.trim();

  if(email && email.match(pattern)) {
    return email;
  } else {
    return null;
  }
}

function validPhone(pNumber) {
  let pattern = /^\d+$/;
  pNumber.trim();

  if(pNumber && pNumber.length <= 10 && pNumber.match(pattern)) {
    return pNumber;
  } else {
    return null;
  }
}

function notEmpty(value) {
  value.trim();

  if(value && value.length > 0) {
    return value;
  } else {
    return null;
  }
}

function validMessage(message) {
  message.trim();

  if(message && message.length >= 50) {
    return message;
  } else {
    return null;
  }
}


mail.handleSendMail = (req, res) => {

  var data = {};

  data.firstName = notEmpty(req.body.firstName);
  data.lastName = notEmpty(req.body.lastName);
  data.phone = validPhone(req.body.phone);
  data.email = validEmail(req.body.email);
  data.message = validMessage(req.body.message);

  if(data.email && data.phone && data.message && data.lastName && data.firstName) {
    client.sendEmail({
      to: 'gowildcastles@gmail.com',
      from: 'noreply@gowildcastlehirebrisbane.com.au',
      subject: 'Website inquiry from ' + data.firstName + ' ' + data.lastName,
      message: 'from - ' + data.firstName + ' ' + data.lastName  + '\n' + data.email + '\n' + data.phone + '\n\n' + 'Message:' + '\n' + data.message
    }, function(error, data, response) {
      if(error) {
        console.log('Message not sent: error recieved');
        console.log(error);
        logger.error(error.Type + ': ' + error.Code);
        res.json({
          sent: false,
          message: 'Message not sent: error recieved',
          error: error
        });
      }

      console.log('Message sent successfully');
      console.log('response: ' + response);
      res.json({
        sent: true,
        message: 'Message sent successfully',
        response: response
      });
    });
  } else {
    console.log('Message not sent: form is invalid');
    res.json({
      sent: false,
      message: 'Message not sent: form is invalid'
    });
  }
};

module.exports = mail;
