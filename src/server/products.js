/*jslint node: true */
'use strict';

var castleData = require('./JSON/castles.json');
var slushieData = require('./JSON/slushies.json');
var popcornData = require('./JSON/popcorn.json');

var products = {};

products.getCastles = function (error, response) {
  response.send(castleData);
};

products.getSlushies = function (error, response) {
  response.send(slushieData);
};

products.getPopcorn = function (error, response) {
  response.send(popcornData);
};

module.exports = products;
