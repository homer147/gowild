/* jslint node: true */
'use strict';

const express = require('express');
const app = express();
const path = require('path');
const keystone = require('keystone');
const	session = require('express-session');
const bodyParser = require('body-parser');
const logger = require('morgan');
const mail = require('./mail');
const products = require('./products');
const config = require('../config/config');
const	multer = require('multer');
const mongoClient = require('mongodb').MongoClient;
const routeUrls = require('./JSON/routes').routes;

var port = process.env.PORT || 8080;
var environment = process.env.NODE_ENV || 'production';
var	cookieSecret = config.cookieSecret;

app.use(session({
	secret: cookieSecret,
  resave: false,
  saveUninitialized: true
}));

// get feature toggle for keystone. (prob pretty useless since i will have to restart app to take effect anyway)
var toggles = {};
mongoClient.connect(config.connectionString, function (err, db) {
  if (err) { throw err; }

  db.collection('toggles').find({ 'name': 'keystone' }).toArray(function (err, result) {
    if (err) { throw err; }

    toggles[result[0].name] = result[0].toggleOn;

    startServer();
    db.close();
  });
});
// end get feature toggle

app.use(bodyParser.urlencoded({	extended: true }));
app.use(bodyParser.json());
// multer for keystone uploads
app.use(multer({ dest: './uploads' }));
// logger for logging to console during dev
app.use(logger('dev'));

keystone.init({
  'name': 'Keystone',
  'brand': 'GoWild',
  'port': 3010,
  'updates': 'updates',
  'auth': true,
  'user model': 'User',
  'auto update': true,
  'cookie secret': cookieSecret,
});

// Let keystone know where the models are defined.
keystone.import('./models');

console.log('About to crank up node');
console.log('PORT=' + port);
console.log('NODE_ENV=' + environment);


function startServer() {
  console.log('** ' + environment.toUpperCase() + ' **');
  app.post('/api/sendMail', mail.handleSendMail);
  app.get('/api/getCastles', products.getCastles);
  app.get('/api/getSlushies', products.getSlushies);
  app.get('/api/getPopcorn', products.getPopcorn);

  switch(environment) {
    case 'build':
      // feature toggle keystone
      if (toggles.keystone) {
        app.use('/keystone', require('./node_modules/keystone/admin/server/app/createStaticRouter.js')(keystone));
        app.use('/keystone', require('./node_modules/keystone/admin/server/app/createDynamicRouter.js')(keystone));
      }
      app.use(express.static('./public/'));
      app.use((req, res) => {
        var fourOhFour = true;

        for (var i = 0; i < routeUrls.length; i++) {
          if (req.url === routeUrls[i]) {
            fourOhFour = false;
            break;
          }
        }

        if (fourOhFour) {
          res.status(404).sendFile(path.join(__dirname, '../../public/index.html'));
        } else {
          res.sendFile(path.join(__dirname, '../../public/index.html'));
        }
      });
      break;
    case 'dev':
      // feature toggle keystone
      if (toggles.keystone) {
        app.use('/keystone', require('./node_modules/keystone/admin/server/app/createStaticRouter.js')(keystone));
        app.use('/keystone', require('./node_modules/keystone/admin/server/app/createDynamicRouter.js')(keystone));
      }
      app.use(express.static('./src/client/'));
      app.use(express.static('./'));
      app.use(express.static('./.tmp/'));
      app.use((req, res) => {
        var fourOhFour = true;

        for (var i = 0; i < routeUrls.length; i++) {
          if (req.url === routeUrls[i]) {
            fourOhFour = false;
            break;
          }
        }

        if (fourOhFour) {
          res.status(404).sendFile(path.join(__dirname, '../client/index.html'));
        } else {
          res.sendFile(path.join(__dirname, '../client/index.html'));
        }
      });
      break;
    default:
      // prod code here
      // feature toggle keystone
      if (toggles.keystone) {
        app.use('/keystone', require('../node_modules/keystone/admin/server/app/createStaticRouter.js')(keystone));
        app.use('/keystone', require('../node_modules/keystone/admin/server/app/createDynamicRouter.js')(keystone));
      }
      app.use(express.static('../public/'));
      app.use((req, res) => {
        var fourOhFour = true;

        for (var i = 0; i < routeUrls.length; i++) {
          if (req.url === routeUrls[i]) {
            fourOhFour = false;
            break;
          }
        }

        if (fourOhFour) {
          res.status(404).sendFile(path.join(__dirname, '../public/index.html'));
        } else {
          res.sendFile(path.join(__dirname, '../public/index.html'));
        }
      });
      break;
  }

  keystone.app = app;
  keystone.start();
}

app.listen(8080, 'localhost', () => {
	console.log('Express listening on port ' + port);
	console.log('env = ' + app.get('env') +
		'\n__dirname = ' + __dirname +
		'\nprocess.cwd = ' + process.cwd());
});
