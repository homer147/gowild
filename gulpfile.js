/*jslint node: true */
'use strict';

var gulp = require('gulp');
var args = require('yargs').argv;
var browserSync = require('browser-sync');
var config = require('./gulp.config')();
var del = require('del');
var port = process.env.PORT || config.defaultPort;
var Server = require('karma').Server;
var $ = require('gulp-load-plugins')({lazy: true});
var swPrecache = require('sw-precache');

gulp.task('help', $.taskListing);
gulp.task('default', ['help']);

gulp.task('wiredep', function() {
	log('Wire up the bower css, lib js and app js into the html');
	var options = config.getWiredepDefaultOptions();
	var wiredep = require('wiredep').stream;

	return gulp
		.src(config.index)
		.pipe(wiredep(options))
		.pipe($.inject(gulp.src(config.js)))
		.pipe(gulp.dest(config.client));
});

gulp.task('inject', ['clean-code', 'styles', 'lint', 'jscs', 'templatecache', 'wiredep', 'test'], function() {
	log('Wire up the app css into the html and call wiredep');
	return gulp
		.src(config.index)
		.pipe($.inject(gulp.src(config.css)))
		.pipe(gulp.dest(config.client));
});

gulp.task('lint', function() {
	log('Linting the js code');
	return gulp
		.src(config.js)
		.pipe($.jshint())
		.pipe($.jshint.reporter('jshint-stylish', {verbose: true, codeCol: 'green'}));
});

gulp.task('jscs', function() {
	log('Checking code against style guide');
	return gulp
		.src(config.js)
		.pipe($.jscs())
		.pipe($.jscs.reporter());
});

gulp.task('fonts', ['clean-fonts'], function() {
	log('Copying fonts');

	return gulp
		.src(config.fonts)
		.pipe(gulp.dest(config.public + 'fonts'));
});

gulp.task('images', ['clean-images'], function() {
	log('Copying and compressing the images');

	return gulp
		.src(config.images)
		.pipe($.imagemin({optimizationLevel: 4}))
		.pipe(gulp.dest(config.public + 'images'));
});

gulp.task('styles', ['clean-styles'], function() {
	log('Compiling Less --> CSS');
	return gulp
		.src(config.less)
		.pipe($.plumber())
		.pipe($.less())
		.pipe($.autoprefixer({browsers: ['last 2 versions', '> 5%']}))
		.pipe(gulp.dest(config.temp));
});

gulp.task('less-watcher', function() {
	log('watching the css for changes');
	gulp.watch([config.lessWatch], ['styles']);
});

gulp.task('templatecache', function(){
	log('Creating AngularJS $templateCache');
	return gulp
		.src(config.htmltemplates)
		.pipe($.htmlmin({collapseWhitespace: true}))
		.pipe($.angularTemplatecache(
			config.templateCache.file,
			config.templateCache.options
		))
		.pipe(gulp.dest(config.temp));
});

gulp.task('service-worker', ['optimise'], function (callback) {
	swPrecache.write(config.public + 'service-worker.js', {
		staticFileGlobs: [config.public + '/**/*.{js,html,css,png,jpg,gif,svg,eot,ttf,woff,woff2,otf}'],
		stripPrefix: config.public
	}, callback);
});

gulp.task('prod', ['service-worker']);

gulp.task('optimise', ['inject', 'images', 'fonts'], function() {
	log('Optimising the javascript, css, html');

	var templateCache = config.temp + config.templateCache.file;
	var cssFilter = $.filter('**/*.css', {restore: true});
	var jsLibFilter = $.filter('**/' + config.optimised.lib, {restore: true});
	var jsAppFilter = $.filter('**/' + config.optimised.app, {restore: true});
	var indexHtmlFilter = $.filter(['**/*', '!**/index.html'], {restore: true});

	return gulp
		.src(config.index)
		.pipe($.plumber())
		.pipe($.inject(gulp.src(templateCache, {read: false}), {
			starttag: '<!-- inject:templates:js -->'
		}))
		.pipe($.useref({searchPath: './'}))
		.pipe(cssFilter)
		.pipe($.csso())
		.pipe(cssFilter.restore)
		.pipe(jsLibFilter)
		.pipe($.uglify())
		.pipe(jsLibFilter.restore)
		.pipe(jsAppFilter)
		.pipe($.uglify())
		.pipe(jsAppFilter.restore)
		.pipe(indexHtmlFilter)
		.pipe($.rev())
		.pipe(indexHtmlFilter.restore)
		.pipe($.revReplace())
		.pipe(gulp.dest(config.public))
		.pipe($.rev.manifest())
		.pipe(gulp.dest(config.public));
});

gulp.task('clean', function() {
	var delconfig = [].concat(config.public, config.temp);
	log('Cleaning: ' + $.util.colors.green(delconfig));
	return del(delconfig);
});

gulp.task('clean-styles', function() {
	return clean(config.temp + '**/*.css');
});

gulp.task('clean-fonts', function() {
	return clean(config.public + 'fonts/**/*.*');
});

gulp.task('clean-images', function() {
	return clean(config.public + 'images/**/*.*');
});

gulp.task('clean-code', function() {
	var files = [].concat(
		config.temp + '**/*.js',
		config.public + '**/*.html',
		config.public + 'js/**/*.js',
		config.public + 'css/**/*.css',
		config.public + 'service-worker.js'
	);
	return clean(files);
});

gulp.task('test', function(done) {
	log('Running Karma');
	return new Server({
		configFile: __dirname + '/karma.conf.js',
		singleRun: true
	}, done).start();
});

gulp.task('spec-watch', function() {
	log('Watching the js for changes so tests can run');
  gulp.watch(config.tests, ['test']);
  gulp.watch(config.js, ['lint', 'jscs', 'test']);
});

/**
 * Bump the verstion
 * --type=pre will bump the prerelease version *.*.*-x
 * --type=patch or no flag will bump the patch version *.*.x
 * --type=minor will bump the minor version *.x.*
 * --type=major will bump the major version x.*.*
 * --version=1.2.3 will bump to a specific version and irnore other flags
 */
gulp.task('bump', function() {
	var msg = 'Bumping versions';
	var type = args.type;
	var version = args.version;
	var options = {};
	if (version) {
		options.version = version;
		msg += ' to ' + version;
	} else {
		options.type = type;
		msg += ' for a ' + type;
	}
	log(msg);
	return gulp
		.src(config.packages)
		.pipe($.print())
		.pipe($.bump(options))
		.pipe(gulp.dest(config.root));
});

gulp.task('serve-build', ['prod'], function() {
	serve(false /* isDev */);
});

gulp.task('serve-dev', ['inject', 'spec-watch'], function() {
	serve(true /* isDev */);
});

//////// FUNCTIONS //////////

function serve(isDev) {
	var nodeOptions = {
		script: config.nodeServer,
		delayTime: 0,
		env: {
			'PORT': port,
			'NODE_ENV': isDev ? 'dev' : 'build'
		},
		watch: [config.server]
	};

	return $.nodemon(nodeOptions)
		.on('restart', function(ev) {
			log('*** nodemon restarted ***');
			log('files changed on restart:\n' + ev);
			setTimeout(function() {
				browserSync.notify('reloading now...');
				browserSync.reload({stream: false});
			}, config.browserReloadDelay);
		})
		.on('start', function() {
			log('*** nodemon started ***');
			startBrowserSync(isDev);
		})
		.on('crash', function() {
			log('*** nodemon crashed ***');
		})
		.on('exit', function() {
			log('*** nodemon exited cleanly ***');
		});
}

function startBrowserSync(isDev) {
	if (args.nosync || browserSync.active) {
		return;
	}

	log('Starting browser-sync on port ' + port);

	if (isDev) {
		gulp.watch([config.lessWatch], ['styles'])
		.on('change', function(event) { changeEvent(event); });
	} else {
		gulp.watch([config.lessWatch, config.js, config.html], ['prod'], browserSync.reload)
		.on('change', function(event) { changeEvent(event); });
	}

	var options = {
		proxy: 'http://localhost:8080/',
		files: isDev ? [
			config.client + '**/*.*',
			config.temp + '**/*.css',
			'!' + config.client + '**/*.less'
		] : [],
		ghostMode: {
			clicks: true,
			location: false,
			forms: true,
			scroll: true
		},
		injectChanges: true,
		logFileChanges: true,
		logLevel: 'debug',
		logPrefix: 'gulp-patterns',
		notify: false
	};
	browserSync(options);
}

function changeEvent(event) {
	var srcPattern = new RegExp('/.*(?=/' + event.source + ')/');
	log('File ' + event.path.replace(srcPattern, '') + ' ' + event.type);
}

function clean(path) {
	log('Cleaning: ' + $.util.colors.green(path));
	return del(path);
}

function log(msg) {
	if (typeof(msg) === 'object') {
		for (var item in msg) {
			if(msg.hasOwnProperty(item)) {
				$.util.log($.util.colors.green(msg[item]));
			}
		}
	} else {
		$.util.log($.util.colors.green(msg));
	}
}
