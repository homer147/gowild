/*jslint node: true */
'use strict';

module.exports = function() {
	var client = './src/client/';
	var clientApp = client + 'app/';
	var root = './';
	var server = './src/server/';
	var temp = './.tmp/';

	var config = {

		// File paths
		client: client,
		css: temp + 'styles.css',
		fonts: ['./bower_components/font-awesome/fonts/**/*.*', client + 'fonts/**/*.*'],
		html: client + '**/*.html',
		htmltemplates: client + '**/*.html',
		images: client + 'images/**/*.*',
		index: client + 'index.html',
		js: [
			clientApp + '**/*.module.js',
			clientApp + '**/*.js',
			'!' + clientApp + '**/*.spec.js',
			'!' + clientApp + 'service-worker.js'
		],
		less: client + '/styles/styles.less',
		lessWatch: [
			client + 'styles/**/**.less',
			clientApp + '**/*.less'
		],
		public: './public/',
		root: root,
		server: server,
		temp: temp,
		tests: clientApp + '**/*.spec.js',

		// Optimised
		optimised: {
			app: 'app.js',
			lib: 'lib.js'
		},

		// Template cache
		templateCache: {
			file: 'templates.js',
			options: {
				module: 'goWildApp',
				standAlone: false,
				root: '/'
			}
		},

		// browserSync
		browserReloadDelay: 1000,

		// Bower and NPM locations
		bower: {
			json: require('./bower.json'),
			directory: './bower_components/',
			ignorePath: '../..',
			exclude: ['bootstrap/dist/js/', 'jquery/']
		},
		packages: [
			'./package.json',
			'./bower.json'
		],
		defaultPort: 51000,
		nodeServer: './src/server/gowild.js'
	};

	config.getWiredepDefaultOptions = function() {
		var options = {
			bowerJson: config.bower.json,
			directory: config.bower.directory,
			ignorePath: config.bower.ignorePath,
			exclude: config.bower.exclude
		};
		return options;
	};

	return config;
};
