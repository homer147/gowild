/*jslint node: true */
'use strict';

module.exports = function(config) {
  config.set({
    basePath: './',
    frameworks: ['jasmine'],
    browsers: ['PhantomJS'],
    colors: true,
    files: [
      // lib files
      './bower_components/jquery/dist/jquery.js',
      './bower_components/angular/angular.js',
      './bower_components/angular-ui-router/release/angular-ui-router.js',
      './bower_components/angular-sanitize/angular-sanitize.js',
      './bower_components/angular-mocks/angular-mocks.js',
      './bower_components/angular-aria/angular-aria.js',
      './node_modules/jasmine-jquery/lib/jasmine-jquery.js',

      // project files
      'src/client/app/app.module.js',
      'src/client/app/gowild-core/gowild-core.js',
      'src/client/app/gowild-contact/gowild-contact.js',
      'src/client/app/gowild-hire/gowild-hire.js',
      'src/client/app/**/*.js',
      'src/client/app/**/*.spec.js',
      'src/client/app/**/*.html',

      // fixtures
        {pattern: 'src/client/app/**/*.spec.json', watched: true, served: true, included: false}
    ],
    reporters: ['spec', 'coverage'],
    coverageReporter: {
      type: 'html',
      dir: 'coverage',
      includeAllSources: true
    },
    preprocessors: {
      'src/client/app/**/!(*spec).js': ['coverage'],
      'src/client/app/**/*.html': ['ng-html2js']
    },
    ngHtml2JsPreprocessor: {
      stripPrefix: 'src/client',
      moduleName: 'templates'
    },
    singleRun: true
  });
};
